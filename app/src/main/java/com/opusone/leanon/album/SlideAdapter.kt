package com.opusone.leanon.album

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.opusone.leanon.oorealmmanager.model.OoRmAlbumPicture
import kotlinx.android.synthetic.main.item_slide.view.*
import android.graphics.drawable.Drawable
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target


class SlideAdapter (var pictureList : MutableList<OoRmAlbumPicture>?)
    : PagerAdapter() {
    private val TAG = "SlideViewAdapter"
    var onPictureShow : OnPictureShow? = null
    var onItemClicked : OnItemClicked? = null

    override fun isViewFromObject(view: View, any: Any): Boolean {
        return view.equals(any)
    }

    override fun getCount(): Int {
        var count = 0
        pictureList?.size?.let {
            count = it
        }
        return count
    }


    override fun destroyItem(container: ViewGroup, position: Int, oo: Any) {
        container.removeView(oo as View)
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater: LayoutInflater = LayoutInflater.from(container.context)
        val imageLayout = inflater.inflate(R.layout.item_slide, container, false)!!
        val imageView = imageLayout.showimage_album
        imageView.maximumScale = 4F
        imageView.setOnClickListener {
            onItemClicked?.let {
                it()
            }
        }
        pictureList?.get(position)?.url?.let {
            Glide.with(container.context)
                .load(it)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        imageLayout.progress_album.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        imageLayout.progress_album.visibility = View.GONE
                        return false
                    }
                })
                .into(imageView)
            onPictureShow?.let {
                it(position)
            }
        }

        container.addView(imageLayout, 0)
        return imageLayout
    }
}
typealias OnPictureShow = (Int) -> Unit
typealias OnItemClicked = () -> Unit