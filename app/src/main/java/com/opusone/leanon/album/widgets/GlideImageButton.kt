package com.opusone.leanon.album.widgets

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.ImageButton

class GlideImageButton @JvmOverloads constructor(context: Context,
                                                 attrs: AttributeSet? = null,
                                                 defStyleAttr: Int = 0): ImageButton(context, attrs, defStyleAttr) {

    private val TAG = "GlideImageButton"

    private var touchStayed = false

    var onClickImageButtonListener: OnGlideCircleButtonClickListener? = null

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val rect = Rect(0, 0, right - left, bottom - top)

        when(event?.action) {
            MotionEvent.ACTION_DOWN -> {
                drawDownStatus()
                touchStayed = false
                return true
            }

            MotionEvent.ACTION_UP -> {
                drawDefaultStatus()
                if (touchStayed) {
                    performClick()
                }
                touchStayed = false

                return true
            }

            MotionEvent.ACTION_CANCEL -> {
                drawDefaultStatus()
                touchStayed = false
                return true
            }

            MotionEvent.ACTION_MOVE -> {
                touchStayed = rect.contains(event.x.toInt(), event.y.toInt())
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    override fun performClick(): Boolean {
        super.performClick()
        onClickImageButtonListener?.let {
            it(this)
        }
        return true
    }

    private fun drawDownStatus() {
        this.alpha = 0.7f
    }

    private fun drawDefaultStatus() {
        this.alpha = 1.0f
    }
}

typealias OnGlideCircleButtonClickListener = (v: View) -> Unit