package com.opusone.leanon.album

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.opusone.leanon.oocontentresolver.OoContentResolver
import com.opusone.leanon.oocontentresolver.model.User
import com.opusone.leanon.oordbobserver.OoRDBObserver
import com.opusone.leanon.oordbobserver.extension.addAlbumObserver
import com.opusone.leanon.oorealmmanager.OoRealmManager
import com.opusone.leanon.oorealmmanager.model.OoRmAlbumPicture
import com.opusone.leanon.oorestmanager.model.OoAlbumPicture
import com.opusone.leanon.oorestmanager.restful.OoRestManager
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_main.*
import com.crashlytics.android.Crashlytics
import com.opusone.leanon.oorestmanager.restful.OoNetworkManager
import com.opusone.leanon.oorestmanager.restful.OoNetworkStatus
import io.fabric.sdk.android.Fabric
import org.jetbrains.anko.longToast
import org.jetbrains.anko.runOnUiThread


class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"

    private var user : User? = null
    private var thumbnailAdapter : ThumbnailAdapter? = null
    private var lastTimestamp = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fabric.with(this, Crashlytics())

        initManagers()
        initWidget()
        addNetwrokChangedObserver()
    }

    private fun initManagers() {
        user = OoContentResolver.getUserInfo(contentResolver)

        OoRealmManager.initRealm(applicationContext)

        OoNetworkManager.init(this)

        OoRestManager.init(true)
        OoRestManager.setReachable(OoNetworkManager) {
            applicationContext.runOnUiThread {
                longToast(R.string.toast_lost_network)
            }
        }
    }

    private fun initWidget() {
        user?.id?.let {id->
            OoRealmManager.getAllAlbumPictures(id)?.let {
                realmInfo = it
                realmInfo?.second?.let { dataSet ->
                    thumbnailAdapter = ThumbnailAdapter(dataSet)
                    thumbnailAdapter?.onClickListener = {
                        startSlideActivity(it)
                    }
                    thumbnailRecyclerView.adapter = thumbnailAdapter
                    thumbnailRecyclerView.adapter?.itemCount?.let {
                        thumbnailRecyclerView.scrollToPosition(0)
                    }
                    attachGroupChatObserver(id, dataSet)
                }
            }
        }
    }

    private fun addNetwrokChangedObserver() {
        OoNetworkManager.onNetworkChangeListener = { status ->
            when (status) {
                OoNetworkStatus.LOST -> {
                    runOnUiThread {
                        longToast(R.string.toast_lost_network)
                    }
                }

                OoNetworkStatus.CELLULAR -> {
                    runOnUiThread {
                        longToast(R.string.toast_available_mobile_network)
                    }
                }

                OoNetworkStatus.WiFi -> {
                    runOnUiThread {
                        longToast(R.string.toast_available_wifi_network)
                    }
                }
            }
        }
    }

    private fun startSlideActivity(position: Int) {
        val intent = Intent(this, SlideActivity::class.java)
        intent.putExtra("position", position)
        startActivity(intent)
    }

    private fun attachGroupChatObserver(albumId : String, dataSet: RealmResults<OoRmAlbumPicture>) {
        if (dataSet.size != 0) {
            dataSet.first()?.timestamp?.let {
                lastTimestamp = it
            }
            OoRDBObserver.addAlbumObserver(albumId, lastTimestamp, ::onAlbumListAdded, ::onAlbumListChanged, ::onAlbumListRemoved)
        } else {
            OoRestManager.getRecentAlbumlList(albumId, 0) { error, response ->
                Log.i(TAG, "error is $error, response is $response")
                response?.albumList?.let {
                    it.forEach { picture ->
                        onAlbumListAdded(picture)
                    }
                    if(dataSet.size != 0) {
                        dataSet.first()?.timestamp?.let {
                            lastTimestamp = it
                        }
                        OoRDBObserver.addAlbumObserver(albumId, lastTimestamp, ::onAlbumListAdded, ::onAlbumListChanged, ::onAlbumListRemoved)
                    } else {
                        noImageText.visibility = View.VISIBLE
                        OoRDBObserver.addAlbumObserver(albumId, lastTimestamp, ::onAlbumListAdded, ::onAlbumListChanged, ::onAlbumListRemoved)
                    }
                }
            }
        }
    }

    private fun onAlbumListAdded(picture: OoAlbumPicture?) {
        Log.d(TAG, "@Album@ onAddedAlbumPicture $picture")

        picture?.let {
            noImageText.visibility = View.GONE
            val rmPicture = OoRmAlbumPicture()
            rmPicture.thumbnail = it.thumbnail
            rmPicture.albumId = user?.id
            rmPicture.authorId = it.authorId
            rmPicture.authorName = it.authorName
            rmPicture.authorPicture = it.authorPicture
            rmPicture.timestamp = it.timestamp
            rmPicture.url = it.url
            OoRealmManager.create(rmPicture)

            lastTimestamp = it.timestamp
            Log.i(TAG, "timestamp is $lastTimestamp")
        }
        thumbnailRecyclerView.smoothScrollToPosition(0)
        thumbnailAdapter?.notifyDataSetChanged()
    }

    private fun onAlbumListChanged(picture: OoAlbumPicture?) {
        Log.d(TAG, "@Album@ onChangedAlbumPicture $picture")
        //TODO: 앨범의 사진 하나가 수정됨.
    }

    private fun onAlbumListRemoved(picture: OoAlbumPicture?) {
        Log.d(TAG, "@Album@ onRemovedAlbumPicture $picture")
        //TODO: 앨범의 사진 하나가 삭제됨
    }

    companion object {
        var realmInfo: Pair<Realm, RealmResults<OoRmAlbumPicture>>? = null
    }
}
