package com.opusone.leanon.album

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_slide.*
import java.text.SimpleDateFormat
import java.util.*

class SlideActivity : AppCompatActivity() {
    private val TAG = "SlideActivity"
    private var NUM_PAGES = 0
    lateinit var slideViewAdapter: SlideAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slide)
        initWidget()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initWidget() {
        leftButton.setOnClickListener {
            ooViewPager.setCurrentItem(--ooViewPager.currentItem, false)
        }
        rightButton.setOnClickListener {
            ooViewPager.setCurrentItem(++ooViewPager.currentItem, false)
        }
        MainActivity.realmInfo?.second?.let {dataSet ->
            if (ooViewPager.currentItem == NUM_PAGES) {
                ooViewPager.currentItem = 0
            }
            intent?.let {
                NUM_PAGES = dataSet.size
                leftButton.isEnabled = !ooViewPager.currentItem.equals(0)
                rightButton.isEnabled = !ooViewPager.currentItem.equals(NUM_PAGES-1)
                slideViewAdapter = SlideAdapter(dataSet.toMutableList())
                slideViewAdapter.onPictureShow = {
                    dataSet.get(it)?.let {
                        pictureInfo.text = "${it.authorName}\n${getTimeString(it.timestamp)}"
                    }
                }
                slideViewAdapter.onItemClicked = {
                    when(imageTopLayout.visibility) {
                        View.GONE -> {
                            imageTopLayout.visibility = View.VISIBLE
                        }
                        View.VISIBLE -> {
                            imageTopLayout.visibility = View.GONE}
                    }
                }
                ooViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                    override fun onPageScrollStateChanged(state: Int) {
                    }

                    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                    }
                    override fun onPageSelected(position: Int) {
                        Log.i(TAG, "left is ${!position.equals(0)}, right is ${!position.equals(NUM_PAGES-1)}")
                        leftButton.isEnabled = !position.equals(0)
                        rightButton.isEnabled = !position.equals(NUM_PAGES-1)
                    }

                })
                ooViewPager.adapter = slideViewAdapter
                ooViewPager.setCurrentItem(it.getIntExtra("position", 0), true)
            }
        }

    }
    fun getTimeString(timestamp : Long?) : String {
        val dateFormat = SimpleDateFormat("yyyy년 MM월 dd일", Locale.getDefault())
        return dateFormat.format(timestamp)
    }
}
