package com.opusone.leanon.album

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.opusone.leanon.oorealmmanager.model.OoRmAlbumPicture
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_thumbnail.view.*

class ThumbnailAdapter (var thumbnailList : OrderedRealmCollection<OoRmAlbumPicture>?) : RealmRecyclerViewAdapter<OoRmAlbumPicture, RecyclerView.ViewHolder>(thumbnailList, true) {
    private val TAG = "ThumbnailAdapter"

    var onClickListener: OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_thumbnail, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val imageView = holder as ViewHolder
        val info = thumbnailList?.get(position)
        info?.let {
            Glide.with(holder.itemView.context)
                .load(it.thumbnail)
                .centerCrop()
                .into(holder.thumbnailButton)
            imageView.thumbnailButton.setOnClickListener {
                onClickListener?.let {
                    it(position)
                    Log.i(TAG, "currentPage send $position")
                }
            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val thumbnailButton = view.item_thumbnail
    }
}

typealias OnClickListener = (pos: Int) -> Unit